;;; ASDF Definition.  -*- lisp -*-

(defpackage :asdf-component-shared-unix-library-system
  (:use :cl :asdf)
  (:export #:shared-unix-library))

(in-package :asdf-component-shared-unix-library-system)

(defclass shared-unix-library (source-file)
  ())

(defmethod source-file-type ((component shared-unix-library) system)
  "so")

(defmethod input-files (operation (component shared-unix-library))
  nil)

(defmethod output-files ((operation compile-op) (component shared-unix-library))
  nil)

(defmethod perform ((operation compile-op) (component shared-unix-library))
  nil)
(defmethod operation-done-p ((o compile-op) (c shared-unix-library))
  t)
(defmethod operation-done-p ((o load-op) (c shared-unix-library))
  nil)

(defmethod perform ((operation load-op) (component shared-unix-library))
  (flet ((load-lib (lib)
	   #+cffi
	   (cffi:load-foreign-library lib)
	   #+(and (not cffi) sbcl)
	   (sb-alien:load-shared-object lib)
	   #+(and (not cffi) allegro)
	   (load lib :foreign t)
	   #-(or sbcl allegro cffi)
	   (error "No foreign library support")))
    (handler-case (load-lib (component-pathname component))
	(error (e) (declare (ignore e))
	       nil))))

(pushnew :asdf-component-shared-unix-library *features*)

(asdf:defsystem :asdf-component-shared-unix-library
  :name "ASDF component :shared-unix-library"
  :components ())

