;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; -*-
;;;
;;; cuddsat.lisp --- SAT solver based on CUDD BDDs

;; Copyright (C) 2009 Utz-Uwe Haus <lisp@uuhaus.de>
;; $Id:$
;; This code is free software; you can redistribute it and/or modify
;; it under the terms of the version 3 of the GNU General
;; Public License as published by the Free Software Foundation, as
;; clarified by the prequel found in LICENSE.Lisp-GPL-Preface.
;;
;; This code is distributed in the hope that it will be useful, but
;; without any warranty; without even the implied warranty of
;; merchantability or fitness for a particular purpose. See the GNU
;; Lesser General Public License for more details.
;;
;; Version 3 of the GNU General Public License is in the file
;; LICENSE.GPL that was distributed with this file. If it is not
;; present, you can access it from
;; http://www.gnu.org/copyleft/gpl.txt (until superseded by a
;; newer version) or write to the Free Software Foundation, Inc., 59
;; Temple Place, Suite 330, Boston, MA 02111-1307 USA
;;
;; Commentary:

;; 

;;; Code:

(eval-when (:compile-toplevel :load-toplevel)
  (declaim (optimize (speed 3) (debug 1) (safety 1))))


(in-package #:CUDD)


(defstruct sat-env 
  (name)
  (manager)
  (cnf)
  (true)
  (false)
  (vars #() :type simple-vector))

(defmacro reference (f)
  (let ((x (gensym)))
    `(let ((,x ,f))
       (cuddapi:cudd-ref ,x)
       ,x)))

(defmacro dereference (f manager)
  (let ((x (gensym)))
    `(let ((,x ,f))
       (cuddapi:cudd-recursive-deref ,manager ,x)
       ,x)))

(deftype cudd-int ()
   `(integer ,#.(1- (- (expt 2 (* 8 (- (cffi:foreign-type-size :int) 1)))))
 	    ,#.(expt 2 (* 8 (- (cffi:foreign-type-size :int) 1)))))

(defun make-clause-tree (env c)
  (loop :with f := (reference (sat-env-false env))
     :with manager := (sat-env-manager env)
     :with vars := (sat-env-vars env)
     :for x :of-type fixnum :in c
     :as var := (svref vars (the cudd-int (abs x)))
     :as phase := (signum x)
     :do (let* ((var (if (= 1 phase)
			 var
			 (cuddapi:cudd-bdd-nand manager var var)))
		(tmp (cuddapi:cudd-bdd-or manager var f)))
	   (reference tmp)
	   (dereference f manager)
	   (setf f tmp))
     :finally (return f)))

(defun sat-env-add-clause (env c)
  (let* ((subtree (make-clause-tree env c))
	 (newcnf (cuddapi:cudd-bdd-and (sat-env-manager env)
				       (sat-env-cnf env) subtree)))
    ;; reference new tree
    (cuddapi:cudd-ref newcnf)
    ;; deref consumed old tree
    (cuddapi:cudd-recursive-deref (sat-env-manager env) (sat-env-cnf env))
    (cuddapi:cudd-recursive-deref (sat-env-manager env) subtree)
    (setf (sat-env-cnf env) newcnf))
  (values))

(defun destroy-sat-env (env)
  (with-slots (name manager cnf vars)
      env
    (declare (type simple-vector vars))
    (format *debug-io* "~&Cleaning up sat-env ~A, tree has ~D nodes~%" name
	    (cuddapi:cudd-dag-size  cnf))
    ;; goes to stderr, unfortunately:
    ;;     (cuddapi:cudd-print-debug manager cnf (length (sat-env-vars env)) 4)
    ;;    (cuddapi:cudd-print-minterm manager cnf)
    
    ;;     (format *debug-io* " ~D refs initially~%"
    ;; 	    (cuddapi:cudd-check-zero-ref manager))
    (cuddapi:cudd-recursive-deref manager cnf)
    (when (= 0 (the cudd-int (cuddapi:cudd-check-zero-ref manager)))
      (break "env ~A empty too early: ~A" name env))

    ;;     (format *debug-io* " destroyed tree, ~D left~%" 
    ;;  	    (cuddapi:cudd-check-zero-ref manager))
    (when (/= (length vars) (the cudd-int (cuddapi:cudd-check-zero-ref manager)))
      (break "env ~A occupation does not match varcount: ~D vs ~D; ~A"
	     name
	     (length vars)
	     (cuddapi:cudd-check-zero-ref manager)
	     env))

    (loop :for x :across vars
       :do (cuddapi:cudd-deref x))
;;      (format *debug-io* " ~D vars deleted, ~D left~%"
;;  	    (length vars) (cuddapi:cudd-check-zero-ref manager))
    (unless (= 0 (the cudd-int (cuddapi:cudd-check-zero-ref manager)))
      (break "env ~A unclean: ~A" name env))
    (unless (= 0 (the cudd-int (cuddapi:cudd-debug-check manager)))
      (break "debug check failed ~D~%" (cuddapi:cudd-debug-check manager)))

    (cuddapi:cudd-quit manager)
;;    (format *debug-io* " done.~%")
    
    ))

(defun make-cnf (numatoms clauses)
  (let* ((name (gensym "cnf-sat-env"))
	 (manager (make-cudd-manager :initial-num-vars (1+ numatoms)))
	 (truevar (cuddapi:cudd-read-one manager))
	 (falsevar (cuddapi:cudd-read-logic-zero manager))
	 (vars (coerce (the list
			 (loop :for i :of-type fixnum :from 0 :upto numatoms
			    :collecting (reference (cuddapi:cudd-bdd-new-var manager))))
		       'simple-vector))
	 (env (make-sat-env :name name
			    :manager manager
			    :cnf truevar
			    :true truevar
			    :false falsevar
			    :vars vars)))
    (cuddapi:cudd-ref truevar) ;; it is used as inital cnf
    (cuddapi:cudd-enable-reordering-reporting manager)
    ;    (cuddapi:cudd-autodyn-enable manager :cudd-reorder-symm-sift)
    ;    (cuddapi:cudd-autodyn-enable manager :cudd-reorder-symm-sift-conv)
    ;    (cuddapi:cudd-autodyn-enable manager :cudd-reorder-sift)
    (cuddapi:cudd-autodyn-enable manager :cudd-reorder-symm-sift)
    (cuddapi:cudd-autodyn-enable manager :cudd-reorder-window-4)
    (cuddapi:cudd-set-max-growth manager 12d0)
    (format T ";; cudd: max sift growth ~D, max number of variables sifted: ~D~%"
	    (cuddapi:cudd-read-max-growth manager)
	    (cuddapi:cudd-read-sift-max-var manager))
    (loop :for c :in clauses
       :with numclauses := (length clauses)
       :for counter :downfrom numclauses
       :do (sat-env-add-clause env c)
       :when (= 0 (mod counter 10))
       :do (format T "~&;; ~D clauses to go~%" counter))
    env))



(defmacro with-sat-bdd ((name numatoms clauses) &body body)
  `(let ((,name (make-cnf ,numatoms ,clauses)))
     (declare (dynamic-extent ,name))
     (unwind-protect
	  (progn ,@body)
       (destroy-sat-env ,name))))

(defun satisfiablep (sat-env f)
  (= 0
     (the cudd-int
       (cuddapi:cudd-bdd-leq (sat-env-manager sat-env) 
			     f (sat-env-false sat-env)))))

(defun get-essential-vars (sat-env)
  "Return a list of two lists: the indices of vars fixed to FALSE and those
fixed to TRUE in the current cnf of sat-env"
  (let ((t-ess '())
	(f-ess '()))
    (loop :with bdd := (reference (sat-env-cnf sat-env))
       :with manager := (sat-env-manager sat-env)
       :for i :of-type fixnum :from 1 :below (length (sat-env-vars sat-env))
       :as var := (svref (sat-env-vars sat-env) i)
       :as negvar := (cuddapi:cudd-bdd-nand manager var var)
       :do (let ((tmp1 (reference
			(cuddapi:cudd-bdd-and manager var bdd)))
		 (tmp2 (reference
			(cuddapi:cudd-bdd-and manager negvar bdd))))
	     (let ((sat-with-T (satisfiablep sat-env tmp1))
		   (sat-with-F (satisfiablep sat-env tmp2)))
	       (cond
		 ((or (and sat-with-T sat-with-F)
		      (and (not sat-with-T) (not sat-with-F)))
		  (dereference tmp1 manager)
		  (dereference tmp2 manager))
		 ((and sat-with-T (not sat-with-F))
		  (push i t-ess)
		  (dereference bdd manager)
		  (dereference tmp2 manager)
		  (setf bdd tmp1))
		 ((and (not sat-with-T) sat-with-F)
		  (push i f-ess)
		  (dereference bdd manager)
		  (dereference tmp1 manager)
		  (setf bdd tmp2)))))
       :finally (dereference bdd manager))
     (list f-ess t-ess)))




(defun satcheck (numatoms clauses)
  "Check the CNF of CLAUSES or satisfiability. 
NUMATOMS is the number of variables occuring in clauses, they are numbered from 1 through NUMATOMS.
CLAUSES is a list of lists, where the sublists are (sparse) disjunctions of atom-indices. Positive/negative sign of index determine phase of literal in the clause.
Return 3 values: T or NIL to answer SAT(clauses), and two lists of fixed-to-1 and fixed-to-0 atoms."
  (with-sat-bdd (bdd numatoms clauses)
    (apply #'values
	   (satisfiablep bdd (sat-env-cnf bdd))
	   (get-essential-vars bdd)) 
    
    ;; (format T "computing...~%")
    ))

(defmacro with-index-hash ((mapping &key (test 'cl:eq)) objects &body body)
  "Execute BODY while binding MAPPING to a hash-table (with predicate
TEST, defaults to #'cl:eq) mapping the OBJECTS to small integers."
  `(let ((,mapping (loop :with ht := (make-hash-table :test ,test)
		     :for x :in ,objects
		      :for num :from 1
		      :do (setf (gethash x ht) num)
		      :finally (return ht))))
     ,@body))

(defun clauses-for-if (lhs rhs)
  "Return clauses for LHS -> RHS, both being disjunctions."
  (mapcar #'(lambda (lhsatom)
	      (cons (- lhsatom) rhs))
	  lhs))

(defun clauses-for-iff (lhs rhs)
  "Return clauses for LHS -> RHS, both being disjunctions."
  (append (clauses-for-if lhs rhs)
	  (clauses-for-if rhs lhs)))

(defun testsat (numvars numclauses &key
		(maxlhs (/ numvars 2))
		(maxrhs (/ numvars 2)))
  (declare (type fixnum numvars numclauses))
  (setf maxlhs (ceiling maxlhs))
  (setf maxrhs (ceiling maxrhs))
  (let ((clauses '()))
    (declare (type fixnum maxlhs maxrhs))
    (loop :repeat numclauses
       :as lhs := (loop :repeat (random maxlhs)
		     :collecting (1+ (random numvars)))
       :as rhs := (loop :repeat (random maxrhs)
		     :collecting (1+ (random numvars)))
       :do (dolist (c (clauses-for-iff lhs rhs))
	     (push c clauses)))
    ;    (format T "~D ~A~%" numvars clauses)
    (satcheck numvars clauses)))