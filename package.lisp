;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; -*-
;;;
;;; package.lisp --- CUDD wrapper package definitions

;; Copyright (C) 2010 Utz-Uwe Haus <lisp@uuhaus.de>
;;
;; $Id:$
;;
;; This code is free software; you can redistribute it and/or modify
;; it under the terms of the version 3 of the GNU General
;; Public License as published by the Free Software Foundation, as
;; clarified by the prequel found in LICENSE.Lisp-GPL-Preface.
;;
;; This code is distributed in the hope that it will be useful, but
;; without any warranty; without even the implied warranty of
;; merchantability or fitness for a particular purpose. See the GNU
;; Lesser General Public License for more details.
;;
;; Version 3 of the GNU General Public License is in the file
;; LICENSE.GPL that was distributed with this file. If it is not
;; present, you can access it from
;; http://www.gnu.org/copyleft/gpl.txt (until superseded by a
;; newer version) or write to the Free Software Foundation, Inc., 59
;; Temple Place, Suite 330, Boston, MA 02111-1307 USA
;;
;; Commentary:

;; 

;;; Code:

;;; Hiding swig:
(cl:defpackage :swig-macros
  (:use :cl :cffi)
  (:documentation
   "Package containing utility functions for SWIG cffi interface generation")
  (:export #:swig-lispify #:defanonenum))

;;; Low-level implementation:
(defpackage #:cuddapi
  (:use :cl :cffi :swig-macros)
  (:export #:cudd-manager #:cudd-node)
  ;; other exports done by swig
  )

;;; Higher-level interface similar to de.uuhaus.bdd
(defpackage #:cudd
  (:use #:cuddapi #:cl)
  (:export #:bdd-environment)
  (:export #:with-bdd-environment #:make-bdd-environment)
  ;  (:export #:make-bdd)
  (:export #:bdd-true #:bdd-false)
  (:export #:bdd-new-variable)
  (:export #:bdd-and #:bdd-or #:bdd-not #:bdd-xor #:bdd-nand #:bdd-nor 
	   #:bdd-and-not #:bdd-or-not #:bdd-implies
	   #:bdd-iff #:bdd-restrict)
  (:export #:bdd-exists #:bdd-forall)
  (:export #:bdd-tautologyp #:bdd-satisfiablep)
  (:export #:bdd-var-min #:bdd-var-max)
  (:export #:bdd-var-fixed0 #:bdd-var-fixed0)
  (:export #:deserialize-bdd)
  (:export #:satcheck)
  (:export #:with-index-hash))