/* swig interface wrapper file for cffi binding generation -*- lisp -*- */
/* swig -cffi interface for CUDD */
/* (c) 2009 Utz-Uwe Haus */

%module "cuddapi"

%feature("intern_function","1");   // use swig-lispify
%feature("export");                // export wrapped things

%insert("lisphead")%{
;;; Auto-generated -*- lisp -*- file
;;; generated from $Id:$
(eval-when (:compile-toplevel :load-toplevel)
  (declaim (optimize (speed 3) (debug 0) (safety 1))))



(cl:in-package :swig-macros)

(cl:defun swig-lispify (name flag cl:&optional (package (find-package :cuddapi)))
      (cl:labels ((helper (lst last rest cl:&aux (c (cl:car lst)))
                    (cl:cond
                      ((cl:null lst)
                       rest)
                      ((cl:upper-case-p c)
                       (helper (cl:cdr lst) 'upper
                               (cl:case last
                                 ((lower digit) (cl:list* c #\- rest))
                                 (cl:t (cl:cons c rest)))))
                      ((cl:lower-case-p c)
                       (helper (cl:cdr lst) 'lower (cl:cons (cl:char-upcase c) rest)))
                      ((cl:digit-char-p c)
                       (helper (cl:cdr lst) 'digit 
                               (cl:case last
                                 ((upper lower) (cl:list* c #\- rest))
                                 (cl:t (cl:cons c rest)))))
                      ((cl:char-equal c #\_)
                       (helper (cl:cdr lst) '_ (cl:cons #\- rest)))
		      ((cl:char-equal c #\-)
                       (helper (cl:cdr lst) '- (cl:cons #\- rest)))
                      (cl:t
                       (cl:error "Invalid character: ~A" c)))))
        (cl:let ((fix (cl:case flag
                        ((constant enumvalue) "+")
                        (variable "*")
                        (cl:t ""))))
          (cl:intern
           (cl:concatenate
            'cl:string
            fix
            (cl:nreverse (helper (cl:concatenate 'cl:list name) cl:nil cl:nil))
            fix)
           package))))


%}

%{

/* includes that SWIG needs to see to parse the cudd.h file go here */

%}

%insert ("swiglisp") %{


(cl:in-package :cuddapi)
%}
%insert ("swiglisp") %{
;; foreign type definitions to hide pointer values
(defclass wrapped-pointer ()
  ((ptrval :reader get-ptrval :initarg :ptrval)
   (ctype  :reader get-ctype :initarg :ctype))
  (:documentation "A wrapped pointer"))

(defmethod print-object ((p wrapped-pointer) stream)
  (print-unreadable-object (p stream :type nil :identity nil)
    (format stream "wrapper around `~A' @0x~16R" (get-ctype p) (get-ptrval p))))

(define-condition foreign-null-pointer-condition (error)
  ((type :initarg :type
	 :reader foreign-null-pointer-condition-type))
  (:report (lambda (condition stream)
	     (format stream "The foreign pointer of type ~A was NULL"
		     (foreign-null-pointer-condition-type condition)))))

%}

%define TYPEMAP_WRAPPED_POINTER(PTRTYPE,LISPCLASS)

%insert ("swiglisp") %{
(define-foreign-type LISPCLASS ()
  ()
  (:actual-type :pointer)
  (:documentation "cffi wrapper type around PTRTYPE."))

(define-parse-method LISPCLASS ()
  (make-instance 'LISPCLASS))

(defmethod translate-from-foreign (value (type LISPCLASS))
  "Wrap PTRTYPE, signaling a null-pointer-exception if value is NULL."
  (if (cffi:null-pointer-p value)
      (error 'foreign-null-pointer-condition :type type)
      (make-instance 'wrapped-pointer :ptrval value :ctype "PTRTYPE")))

(defmethod translate-to-foreign ((g wrapped-pointer) (type LISPCLASS))
  "Unwrap PTRTYPE"
  (get-ptrval g))

%}
%typemap (cin) PTRTYPE "LISPCLASS"
%typemap (cout) PTRTYPE "LISPCLASS"
%enddef

TYPEMAP_WRAPPED_POINTER(DdManager *, cudd-manager)
TYPEMAP_WRAPPED_POINTER(DdNode *, cudd-node)

%typemap (cin)  DdManager * "cudd-manager"
%typemap (cout) DdManager * "cudd-manager"
%typemap (cin)  DdNode * "cudd-node"
%typemap (cout) DdNode * "cudd-node"

/* Now parse and wrap the API header */
%insert ("swiglisp") %{
(eval-when (:compile-toplevel :load-toplevel)
  ;; Muffle compiler-notes globally
  #+sbcl (declaim (sb-ext:muffle-conditions sb-ext:defconstant-uneql))
  
%}

%include "cudd/cudd.h"
%insert ("swiglisp") %{
) ;; end of eval-when to avoid top-level export 
%}

%insert ("swiglisp") %{
;; (defmethod translate-from-foreign :around (manager (type cudd-manager))
;;   (let ((cudd-manager (call-next-method)))
;;     (when manager
;;       (trivial-garbage:finalize cudd-manager #'(lambda () 
;; 						 ;; (break "finalize manager #x~x~%" manager)
;; 						 					      ;; (format T "kill mgmr ~A~%" manager) (force-output)
						 
						 
						 
;; 						 (format T "~D nodes unfreed in manager ~A" (cuddapi:cudd-check-zero-ref manager) manager)
;; 						 ;;						 (cuddapi:cudd-quit manager)
;; ;; 						 					      (format T "killed mgmr ~A~%" manager) (force-output)
;; 						 )))
    
;;     cudd-manager))
%}
%insert ("swiglisp") %{
;; (defmethod translate-from-foreign :around (node (type cudd-node))
;;   (let ((cudd-node (call-next-method)))
;;     (when node
;;       (trivial-garbage:finalize cudd-node #'(lambda () 
;;  					      ;; (format T "finalize node #x~x~%" node)
;;  					      ;; (format T "kill node ~A" node) (force-output)

;; ;;					      (format T "~&killed ~A~%" node)
;; 					      ;;					      (format T "killed node ~A~%" node) (force-output)
;; 					      )))
;;     cudd-node))
%}




%insert ("swiglisp") %{


(cl:in-package :cudd)


;; for garbage collection purposes we wrap a reference to the manager with each
;; variable object
(defvar *bdd-env* nil
  "A special variable holding the current BDD Environment.")

(deftype wrapped-bdd ()
  `(simple-vector 2))

(defmacro wrap-bdd (bdd env)
  "Build a lisp object object around the cudd-node v."
  `(the wrapped-bdd
     (make-array 2 :adjustable NIL :fill-pointer NIL
 		 :initial-contents 
 		 (list ,bdd ,env)))
  )

(defmacro unwrap-bdd (bdd)
  "Extract the cudd-node object from bdd."
  `(svref (the wrapped-bdd ,bdd) 0)
  )


(defun make-cudd-manager (&key
			  (initial-num-vars 0)
			  (initial-num-slots 256) ;; #.(cuddapi:CUDD-UNIQUE-SLOTS)
			  (cache-size 262144) ;; #.(cuddapi:CUDD-CACHE-SLOTS)
			  (max-memory 0))
  (cuddapi:cudd-init initial-num-vars
		     0  ;; num-zvars
		     initial-num-slots
		     cache-size
		     max-memory))

(defstruct (bdd-environment (:constructor construct-bdd-environment))
  "A wrapper object describing a CUDD-based BDD environment."
  (manager )
  (name "" :type string)
  ;; caching of the T and F objects of this manager to save FF overhead
  (true)
  (false)
  ;; mapping VARs to foreign IDs
  (var->id (make-hash-table :test #'eq) :type hash-table)
  (nextvar 0 :type (integer 0 #.(expt 2 (* 8 (- (cffi:foreign-type-size :int) 1))))))

(defmethod print-object ((e bdd-environment) stream)
  (print-unreadable-object (e stream :type T)
    (format stream "~A (manager @#x~X, ~D vars)" 
	    (bdd-environment-name e)
	    (cuddapi::get-ptrval (bdd-environment-manager e))
	    (bdd-environment-nextvar e))))

(defun make-bdd-environment (&key
			     (manager (make-cudd-manager))
			     (name (format nil "BDD Environment ~A" (gensym "BDDE"))))
  (let ((res (construct-bdd-environment :manager manager :name name)))
    (setf (bdd-environment-true res)
	  (wrap-bdd (cuddapi:cudd-read-one manager) res))
    (setf (bdd-environment-false res)
	  (wrap-bdd (cuddapi:cudd-read-logic-zero manager) res))
    ;; these need no finalization:
    (trivial-garbage:cancel-finalization (unwrap-bdd (bdd-environment-false res)))
    (trivial-garbage:cancel-finalization (unwrap-bdd (bdd-environment-true res)))

    
    (let ((vartab (bdd-environment-var->id res)))
      (trivial-garbage:finalize res 
 				#'(lambda ()
 				    (format T "~&killing ~D vars in ~A~%"
					    (hash-table-size vartab)
					    manager)
				    (maphash #'(lambda (key val)
 						 (declare (ignore val))
						 (cuddapi:cudd-recursive-deref
						  manager
						  (unwrap-bdd key)))
					     vartab))))
    res))

(eval-when (:load-toplevel)
  (setf *bdd-env* (or *bdd-env*
  		      (make-bdd-environment :name "The global BDD Environment."))))

(defmacro with-bdd-environment (env &body body)
  "Execute BODY within the bdd environment ENV."
  `(let ((*bdd-env* ,env))
     ,@body))

;;; utility functions for the environment
(defun var->id (var &optional (env *bdd-env*))
  "Return the ID for VAR in ENV (which defaults to *bdd-env*)."
  (gethash var (bdd-environment-var->id env)))

(defsetf var->id (var &optional (env *bdd-env*)) (id)
  `(setf (gethash ,var (bdd-environment-var->id ,env)) ,id))

(defun id->var (id &optional (env *bdd-env*))
  "Return the VAR for ID  in ENV (which defaults to *bdd-env*)."
  (wrap-bdd (cuddapi:cudd-bdd-ith-var (bdd-environment-manager env) id) env))


(defun bdd-true ()
  (bdd-environment-true *bdd-env*))

(defun bdd-false ()
  (bdd-environment-false *bdd-env*))

(defun bdd-new-variable ()
  (handler-case
      (let ((v (wrap-bdd
		(cuddapi:cudd-bdd-new-var (bdd-environment-manager *bdd-env*))
		*bdd-env*))
	    (id (bdd-environment-nextvar *bdd-env*)))
	(progn
	  (incf (bdd-environment-nextvar *bdd-env*))
	  (setf (var->id v *bdd-env*) id)
	  v))
    (cuddapi::foreign-null-pointer-condition (c)
      (declare (ignore c))
      ;; FIXME: maybe try a garbage collection here
      (error "CUDD failed to allocate a variable ~D:~%BDD environment ~A full." 
	     (bdd-environment-nextvar *bdd-env*)
	     *bdd-env*))))

(defun bdd-restrict (var val bdd)
  (wrap-bdd
   (cuddapi:cudd-bdd-restrict (bdd-environment-manager *bdd-env*)
			      (unwrap-bdd bdd)
			      (unwrap-bdd (if val var (bdd-not var))))
   *bdd-env*))

(defun bdd-and (x y)
  (wrap-bdd
   (cuddapi:cudd-bdd-and (bdd-environment-manager *bdd-env*) 
			 (unwrap-bdd x) (unwrap-bdd y))
   *bdd-env*))

(defun bdd-or (x y)
  (wrap-bdd
   (cuddapi:cudd-bdd-or (bdd-environment-manager *bdd-env*) 
			(unwrap-bdd x) (unwrap-bdd y))
   *bdd-env*))

(defun bdd-not (x)
  ;; FIXME: cudd-not is a macro and hence not swig'ed, but would probably 
  ;; perform be better
  (wrap-bdd
   (cuddapi:cudd-bdd-nand (bdd-environment-manager *bdd-env*)
			  (unwrap-bdd x)
			  (unwrap-bdd x))
   *bdd-env*))

(defun bdd-xor (x y)
  (wrap-bdd
   (cuddapi:cudd-bdd-xor (bdd-environment-manager *bdd-env*) 
			 (unwrap-bdd x)
			 (unwrap-bdd y))
   *bdd-env*))

(defun bdd-nand (x y)
  (wrap-bdd
   (cuddapi:cudd-bdd-nand (bdd-environment-manager *bdd-env*) 
			  (unwrap-bdd x) 
			  (unwrap-bdd y))
   *bdd-env*))

(defun bdd-nor (x y)
  (wrap-bdd
   (cuddapi:cudd-bdd-nor (bdd-environment-manager *bdd-env*) 
			 (unwrap-bdd x)
			 (unwrap-bdd y))
   *bdd-env*))

(defun bdd-and-not (bdd1 bdd2)
  (bdd-and bdd1 (bdd-not bdd2)))

(defun bdd-or-not (bdd1 bdd2)
  (bdd-or bdd1 (bdd-not bdd2)))

(defun bdd-implies (bdd1 bdd2)
  (bdd-or-not bdd1 bdd2))

(defun bdd-iff (bdd1 bdd2)
  (bdd-and (bdd-implies bdd1 bdd2)
	   (bdd-implies bdd2 bdd1)))

(defun bdd-equal (bdd1 bdd2)
  "Check whether BDD1 and BDD2 are equal."
  (let ((b1 (unwrap-bdd bdd1))
	(b2 (unwrap-bdd bdd2)))
    (and (= 1 (cuddapi:cudd-bdd-leq (bdd-environment-manager *bdd-env*) b1 b2))
	 (= 1 (cuddapi:cudd-bdd-leq (bdd-environment-manager *bdd-env*) b2 b1)))))

(defun bdd-tautologyp (bdd &optional (fixings '()))
  "Check whether BDD is tautologic, possibly under a given fixing in FIXINGS, a list of pairs (bddvar . value), value being T or NIL. "
  (if fixings
      (bdd-tautologyp (bdd-restrict (caar fixings) (cadr fixings) bdd)
		      (cdr fixings))
      (bdd-equal bdd (bdd-true))))

(defun bdd-satisfiablep (bdd &optional (fixings '()))
  "Check whether BDD is satisfiable, possibly under a given fixing in FIXINGS, a list of pairs (bddvar . value), value being T or NIL. "
  (if fixings
      (bdd-satisfiablep (bdd-restrict (caar fixings) (cadr fixings) bdd)
			(cdr fixings))
      (not (bdd-equal bdd (bdd-false)))))

(defun bdd-var-fixed0 (bdd var)
  (ecase (cuddapi:cudd-bdd-is-var-essential (bdd-environment-manager *bdd-env*)
					    bdd
					    (var->id var *bdd-env*)
					    0)
    (1 T)
    (0 NIL)))

(defun bdd-var-fixed1 (bdd var)
  (ecase (cuddapi:cudd-bdd-is-var-essential (bdd-environment-manager *bdd-env*)
					    bdd
					    (var->id var)
					    1)
    (1 T)
    (0 NIL)))

(defgeneric deserialize-bdd (source)
  (:documentation "Parse a textual representation of a BDD from SOURCE within the current BDD environment."))

(defmethod deserialize-bdd ((s string))
  (with-input-from-string (f s)
    (deserialize-bdd f)))

(defmethod deserialize-bdd ((s pathname))
  (with-open-file (f s :direction :input)
    (deserialize-bdd f)))

(defmethod deserialize-bdd ((l list))
  (with-input-from-string (f (format nil "~W" l))
    (deserialize-bdd f)))

(defmethod deserialize-bdd ((s stream))
  "Read a textual external representation of a bdd from STREAM.
New variables are allocated in the current bdd environment.
Returns two values: the bdd structure and an alist mapping variable numbers to the
variable names specified in the bdd file."
  (labels ((parse-bddfile (s)
             (let* ((bddblock (read s))
                    (data (member "BDD" bddblock :test #'string-equal) )
                    (tree nil)
                    (nodenames nil))
               (unless data
                 (error "Could not find BDD block in ~A" data))
               (setf data (cdr data))
               (setf tree (assoc "DAG" data :test #'string-equal))
               (unless tree 
                 (error "Could not find BDD DAG in ~A" data))
               (setf tree (cadr tree))
               (setf nodenames (assoc "VARNAMES" data :test #'string-equal))
               (unless nodenames 
                 (error "Could not find node name mapping in ~A" data))
               (setf nodenames (cadr nodenames))
               (values tree nodenames)))
           (max-varnum (tree)
             ;; we depend on having consecutive variable numbers in the bdd file
             ;; starting above 0
             ;; Deducing maxvarnum from tree instead of varnames is safer because some
             ;; variable may be missing a name. In that case we fail softly: the tree will be
             ;; fine, but the mapping returned to the caller will be defective in the same way
             ;; that the user messed it up when calling serialize-bdd with an incomplete name map.
             (if (consp tree)
                 (max (second tree)
                      (max-varnum (third tree))
                      (max-varnum (fourth tree)))
                 0)))
    (multiple-value-bind (tree nodenames)
        (parse-bddfile s)
      (let* ((maxvar (max-varnum tree))
             (mapping (make-hash-table :size maxvar :test #'eql)))
	;; fetch new variables
	(loop :for i :from 2 :upto maxvar
	   :do (setf (gethash i mapping) (bdd-new-variable)))
        (labels ((walk (bddtext)
                   (cond
                     ((eq bddtext NIL)
                      (bdd-false))
                     ((eq bddtext T)
                      (bdd-true))
                     (T
		      (let ((thisvar (gethash (second bddtext) mapping))
			    (t-child (walk (third bddtext)))
			    (nil-child (walk (fourth bddtext))))
			(bdd-or (bdd-and thisvar t-child)
				(bdd-and (bdd-not thisvar) nil-child)))))))
          (values
           (walk tree)
           (loop :for pair :in nodenames
              :as oldnum := (car pair)
              :do (setf (car pair)
			(gethash oldnum mapping))
              :finally (return nodenames))))))))


%}
