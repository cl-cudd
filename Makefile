SWIG=swig

all: dist cuddapi.lisp
	
dist:
	make -Cdistr

cuddapi.lisp: cudd-cffi.i
	$(SWIG) -cffi -Idistr $<
