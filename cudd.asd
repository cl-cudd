;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; -*-
;;;
;;; cudd.asd --- cudd wrapper

;; Copyright (C) 2009 Utz-Uwe Haus <lisp@uuhaus.de>
;;
;; $Id:$
;;
;; This code is free software; you can redistribute it and/or modify
;; it under the terms of the version 3 of the GNU General
;; Public License as published by the Free Software Foundation, as
;; clarified by the prequel found in LICENSE.Lisp-GPL-Preface.
;;
;; This code is distributed in the hope that it will be useful, but
;; without any warranty; without even the implied warranty of
;; merchantability or fitness for a particular purpose. See the GNU
;; Lesser General Public License for more details.
;;
;; Version 3 of the GNU General Public License is in the file
;; LICENSE.GPL that was distributed with this file. If it is not
;; present, you can access it from
;; http://www.gnu.org/copyleft/gpl.txt (until superseded by a
;; newer version) or write to the Free Software Foundation, Inc., 59
;; Temple Place, Suite 330, Boston, MA 02111-1307 USA
;;
;; Commentary:

;; 

;;; Code:

#-asdf-component-shared-unix-library
(asdf:operate 'asdf:load-op :asdf-component-shared-unix-library)

(defpackage #:cudd-system
  (:use #:cl #:asdf #:asdf-component-shared-unix-library-system))
(in-package #:cudd-system)

(defsystem #:cudd
  :description "?"
  :version     "0"
  :author      "Utz-Uwe Haus <lisp@uuhaus.de>"
  :license     "ask me"
  :depends-on  (:cffi :trivial-garbage)
  :components  ((:module "foreignlib"
			 :if-component-dep-fails :try-next
			 :pathname "distr/"
			 :components
			 (;; try installed copy
			  (:shared-unix-library "installed_libcudd"
						:pathname "/usr/local/lib/libcudd")
			  (:shared-unix-library "installed_libcudd_so"
						:pathname "/usr/local/lib/libcudd.so")
			  ;; try local copy
			  (:shared-unix-library "temp_libcudd"
						:pathname "./cudd/libcudd")
			  (:shared-unix-library "temp_libcudd_so"
						:pathname "./cudd/libcudd.so")))

		;;		(:file "package")
		(:file "package")
		(:file "cuddapi" :depends-on ("foreignlib" "package"))
		(:file "cuddsat" :depends-on ("cuddapi" "package"))
                ))

